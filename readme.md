
# Training

Presentations, demos and training projects about design, frontend development and web technologies.

- **Bitbucket**: [bitbucket.org/Eiskis/training](https://bitbucket.org/Eiskis/training)
- **Clone**: [https://Eiskis@bitbucket.org/Eiskis/training.git]()
